jQuery(document).on('click', '#agreeclick', function() {
  jQuery('body').addClass('no-height');
  });


function scrollPage(speed) {
  setTimeout(function () {
    $("html, body").animate(
      {
        scrollTop: $(document).height(),
      },
      speed
    );
  }, 3500);
}
$(document).ready(function () {
  // Bg slider
  var slideIndex = 0;
  var bgLength = 10;
  setInterval(function () {
    if (slideIndex == bgLength) {
      slideIndex = 0;
    }
    slideIndex++;
    $(".pics-slider ul li")
      .eq(slideIndex)
      .addClass("active")
      .siblings()
      .removeClass("active");
  }, 3000);
  // Next step
  $('[data-btn="next"]').click(function (e) {
    var qLength = $(".step").length;
    var curIndex = $(this).closest(".step").index() + 1;
    if (curIndex < qLength) {
      $(this).closest(".step").hide().next().fadeIn();
      scrollPage(2000);
    }
    e.preventDefault();
  });
});
$(window).load(function () {
  scrollPage(2000);
});
